#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include <dirent.h>

static  const  char *dirpath = "/home/[user]/Documents";
static  const  char *dirpathAnime = "/home/[user]/Documents/Animeku_";
static struct fuse_operations xmp_oper={
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_mkdir (const char *name, mode_t dirMode)
{
	update_log("MKDIR", name);
	char *path = (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	mkdir(path, dirMode);

	return 0;
}

static int xmp_rmdir (const char *name)
{
	char *nameFold = (char*) malloc(strlen(name));
	strcpy(nameFold, name);
	update_log("RMDIR", nameFold);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	rmtree(path);
	return 0;
}


static int xmp_rename(const char *start, const char *end)
{
    int res;
    char startDir[1000], enddir[1000];
    char *p_startDir, *p_enddir;
    int foldStat = strncmp(end + 1, "IAN_", 4);

    if (strcmp(start, "/") == 0)
    {
        start = dirpath;
        sprintf(startDir, "%s", start);
    }
    else 
    {
        sprintf(startDir, "%s%s", dirpath, start);
    }

    if (strcmp(source, "/") == 0)
    {
        sprintf(enddir, "%s", dirpath);
    }
    else
    {
        sprintf(enddir, "%s%s", dirpath, end);
    }

    res = rename(startDir, enddir);
    if (res == -1)
    {
        return -errno;
    }

    p_startDir = strrchr(startDir, '/');
    p_enddir = strrchr(enddir, '/');

    if (strstr(p_enddir, "Animeku_"))
    {
        logged("RENAME", "terenkripsi", startDir, enddir);
    }
    if (strstr(p_startDir, "Animeku_"))
    {
        logged("RENAME", "terdecode", startDir, enddir);
    }

    DIR *dp;
    struct dirent *de;

    dp = opendir(enddir);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {

    	if(strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, enddir);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath, foldStat);

    			memset(newpath, 0, strlen(newpath));
            }

    		char path_1[1024];
    		char path_2[1024];
    		char newName[50];

    		strcpy(newName, de->d_name);
    		(foldStat == 0) ? vig_cip(newName) : de_vig_cip(newName);
    		
    		strcpy(path_2, enddir);
    		strcat(path_2, "/");
    		strcat(path_2, newName);
    		
    		strcpy(path_1, enddir);
    		strcat(path_1, "/");
    		strcat(path_1, de->d_name);

    		rename(path_1, path_2);
    		
    		memset(path_1, 0, strlen(path_1));
    		memset(path_2, 0, strlen(path_2));
    		memset(newName, 0, strlen(newName));
		}
    }

    closedir(dp);

    return 0;
}

void readSubdir(const char *path, int resStat)
{
	DIR *dp;
    struct dirent *de;

    dp = opendir(path);

    if (dp == NULL) return;

    while ((de = readdir(dp)) != NULL) {

    	if(strcmp(de->d_name,".") && strcmp(de->d_name, ".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, path);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath,resStat);

    			memset(newpath, 0, strlen(newpath));
            }

    		char path_1[1024];
    		char path_2[1024];
    		char newName[50];

    		strcpy(newName, de->d_name);
    		(resStat == 0) ? vig_cip(newName) : de_vig_cip(newName);
    		
    		strcpy(path_2, path);
    		strcat(path_2, "/");
    		strcat(path_2, newName);
    		
    		strcpy(path_1, path);
    		strcat(path_1, "/");
    		strcat(path_1, de->d_name);

    		rename(path_1, path_2);
    		
    		memset(path_1, 0, strlen(path_1));
    		memset(path_2, 0, strlen(path_2));
    		memset(newName, 0, strlen(newName));
		}
    }

    closedir(dp);

    return;
}

void rmtree(const char path[])
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    stat(path, &stat_path);

    if (S_ISDIR(stat_path.st_mode) == 0) {
        fprintf(stderr, "%s: %s\n", "Is not directory", path);
        exit(-1);
    }

    if ((dir = opendir(path)) == NULL) {
        fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
        exit(-1);
    }

    path_len = strlen(path);

    while ((entry = readdir(dir)) != NULL) {

        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        full_path = calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        stat(full_path, &stat_entry);

        if (S_ISDIR(stat_entry.st_mode) != 0) {
            rmtree(full_path);
            continue;
        }

        if (unlink(full_path) == 0)
            printf("Removed a file: %s\n", full_path);
        else
            printf("Can`t remove a file: %s\n", full_path);
        free(full_path);
    }

    if (rmdir(path) == 0)
        printf("Removed a directory: %s\n", path);
    else
        printf("Can`t remove a directory: %s\n", path);

    closedir(dir);
}

int make_log_file()
{
	FILE* file_ptr = fopen(dirLog, "a");
    fclose(file_ptr);

    return 0;
}

int update_log(char *arg, const char *desc)
{
	time_t rawtime;
	struct tm *info;

	time( &rawtime );
	info = localtime( &rawtime );
	
	char infoF[10];
	if(strcmp(arg, "RMDIR") == 0)
	{
		strcpy(infoF, "WARNING");
	}
	else
	{
		strcpy(infoF, "INFO");
	}
	
	FILE* file_ptr = fopen(dirLog, "a");	
	fprintf(file_ptr, "%s::%d%d%d-%02d:%02d:%02d::%s::%s\n", infoF, info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, arg, desc);
	
    fclose(file_ptr);

    return 0;
}

int update_log_rename(const char *desc1, const char *desc2)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	FILE* file_ptr = fopen(dirLog, "a");	
	
	fprintf(file_ptr, "INFO::%d%d%d-%02d:%02d:%02d::RENAME::%s::%s\n", info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, desc1, desc2);
	
    fclose(file_ptr);
    return 0;
}


char *rot13(const char *filename)
{
    if(src == NULL){
      return NULL;
    }
  
    char* result = malloc(strlen(src));
    
    if(result != NULL){
      strcpy(result, src);
      char* current_char = result;
      
      while(*current_char != '\0'){
        //Only increment alphabet characters
        if((*current_char >= 97 && *current_char <= 122) || (*current_char >= 65 && *current_char <= 90)){
          if(*current_char > 109 || (*current_char > 77 && *current_char < 91)){
            //Characters that wrap around to the start of the alphabet
            *current_char -= 13;
          }else{
            //Characters that can be safely incremented
            *current_char += 13;
          }
        }
        current_char++;
      }
    }
    return result;
}

char* atbashCipher(char* filename){
    char normal[26]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    char cipher[26]={'z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h','g','f','e','d','c','b','a'};
    char final[256];
    int buffer;
    int size=sizeof(filename)/sizeof(filename[0]);
    for(int j=0; j<size; j++){
        for(int i=0; i<25; i++){
            if(filename[j]==normal[i]){
                buffer=i;
            }
        }
        final[j]=cipher[i]
    }
}

// void split(char* filename){
//     char delim='/';
//     char* splitted;
//     splitted=strtok(filename,delim);//the first substring
//     while(splitted!=null){
//         splitted=strtok(NULL, delim);//the next substring
//     }
// }

// bool isDirAnime(){
//     DIR *d;
//     struct dirent *dir;
//     bool isAnime=false;
//     d = opendir(dirpath);
//     if (d)
//     {
//         while ((dir = readdir(d)) != NULL)
//         {
//             //printf("%s\n", dir->d_name);
//             if(strstr(dir->d_name,"Animeku_")){
//                 isAnime=true;
//             }
//         }
//         closedir(d);
//         return isAnime;
//     }
// }

bool isStringAllCapOnaStackJustFaxNoPrinter(char* filename){
int size=sizeof(filename)/sizeof(filename[0]);
bool final=true;
for(int j=0; j<size;j++){
    if(isupper(filename[j])==0){
        final=false;
    }
}
return final;
}

static int do_rename(const char *before, const char *after){
    if(strcmp(after,"Animeku_")==0){
    DIR *d;
    char* ciphered;
    struct dirent *dir;
    d=opendir(dirpathAnime);
    if(d){
        while((dir=readdir(d))!=NULL){
            if(isStringAllCapOnaStackJustFaxNoPrinter(dir->d_name)==true){
               ciphered=atbashChiper(dir->d_name);
            }
            else{
                ciphered=rot13(dir->d_name);
            }
            rename(dir->d_name,chipered);
        }
    }
    }
}

void vig_cip(char *word)
{
	char key[10] = "INNUGANTENG";
	int input_len = strlen(word);

	for(int i = 0; i < input_len; i++)
	{
		int j = i % 11;

		if(word[i] >= 'A' && word[i] <= 'Z')
		{
			word[i] = (((word[i] - 65) + (key[j] - 65)) % 26) + 65;
		}

		if(word[i] >= 'a' && word[i] <= 'z')
		{
			word[i] = (((word[i] - 97) + (key[j] - 65)) % 26) + 97;
		}
	}
}

void de_vig_cip(char *word)
{
	
	char key[10] = "INNUGANTENG";
	int input_len = strlen(word);

	for(int i = 0; i < input_len; i++)
	{
		int j = i % 11;

		if(word[i] >= 'A' && word[i] <= 'Z')
		{
			word[i] = (((word[i] - 65) - (key[j] - 65) + 26) % 26) + 65;
		}

		else if(word[i] >= 'a' && word[i] <= 'z')
		{
			word[i] = (((word[i] - 97) - (key[j] - 65) + 26) % 26) + 97;
		}
	}
}


int main(){
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}