# soal-shift-sisop-modul-4-E05-2022


```
M Fathurrahman Al Faiz - 5025201023
Burhanudin Rifa Pradana - 5025201191
Marsyavero Charisyah Putra - 5025201122
```

## Soal 2

### 2A

Pertama kita harus membuat fungsi encode fan decode Vigenere Cipher. Dengan key “INNUGANTENG”

```c
void vig_cip(char *word)
{
	char key[10] = "INNUGANTENG";
	int input_len = strlen(word);

	for(int i = 0; i < input_len; i++)
	{
		int j = i % 11;

		if(word[i] >= 'A' && word[i] <= 'Z')
		{
			word[i] = (((word[i] - 65) + (key[j] - 65)) % 26) + 65;
		}

		if(word[i] >= 'a' && word[i] <= 'z')
		{
			word[i] = (((word[i] - 97) + (key[j] - 65)) % 26) + 97;
		}
	}
}

void de_vig_cip(char *word)
{
	
	char key[11] = "INNUGANTENG";
	int input_len = strlen(word);

	for(int i = 0; i < input_len; i++)
	{
		int j = i % 11;

		if(word[i] >= 'A' && word[i] <= 'Z')
		{
			word[i] = (((word[i] - 65) - (key[j] - 65) + 26) % 26) + 65;
		}

		else if(word[i] >= 'a' && word[i] <= 'z')
		{
			word[i] = (((word[i] - 97) - (key[j] - 65) + 26) % 26) + 97;
		}
	}
}
```

Cipher ini bekerja dengan menambahkan huruf input dengan huruf pada key yang bergantian nantinya. Decode sama saja namun di kurangkan

### 2B dan 2C

Disuruh aktifkan code ini jika rename

```c
static int xmp_rename(const char *start, const char *end)
{
    int res;
    char startDir[1000], enddir[1000];
    char *p_startDir, *p_enddir;
    int foldStat = strncmp(end + 1, "IAN_", 4);

    if (strcmp(start, "/") == 0)
    {
        start = dirpath;
        sprintf(startDir, "%s", start);
    }
    else 
    {
        sprintf(startDir, "%s%s", dirpath, start);
    }

    if (strcmp(source, "/") == 0)
    {
        sprintf(enddir, "%s", dirpath);
    }
    else
    {
        sprintf(enddir, "%s%s", dirpath, end);
    }

    res = rename(startDir, enddir);
    if (res == -1)
    {
        return -errno;
    }

    p_startDir = strrchr(startDir, '/');
    p_enddir = strrchr(enddir, '/');

    if (strstr(p_enddir, "Animeku_"))
    {
        logged("RENAME", "terenkripsi", startDir, enddir);
    }
    if (strstr(p_startDir, "Animeku_"))
    {
        logged("RENAME", "terdecode", startDir, enddir);
    }

    DIR *dp;
    struct dirent *de;

    dp = opendir(enddir);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {

    	if(strcmp(de->d_name, ".") && strcmp(de->d_name, ".."))
    	{
    		if (de->d_type == DT_DIR)
            {
                char newpath[1024];

                strcpy(newpath, enddir);
                strcat(newpath, "/");
                strcat(newpath, de->d_name);

                readSubdir(newpath, foldStat);

    			memset(newpath, 0, strlen(newpath));
            }

    		char path_1[1024];
    		char path_2[1024];
    		char newName[50];

    		strcpy(newName, de->d_name);
    		(foldStat == 0) ? vig_cip(newName) : de_vig_cip(newName);
    		
    		strcpy(path_2, enddir);
    		strcat(path_2, "/");
    		strcat(path_2, newName);
    		
    		strcpy(path_1, enddir);
    		strcat(path_1, "/");
    		strcat(path_1, de->d_name);

    		rename(path_1, path_2);
    		
    		memset(path_1, 0, strlen(path_1));
    		memset(path_2, 0, strlen(path_2));
    		memset(newName, 0, strlen(newName));
		}
    }

    closedir(dp);

    return 0;
}
```

### 2D

Membuat log file

```c
int make_log_file()
{
	FILE* file_ptr = fopen(dirLog, "a");
    fclose(file_ptr);
    return 0;
}
```

### 2E

Log file lanjutan.

```c
void rmtree(const char path[])
{
    size_t path_len;
    char *full_path;
    DIR *dir;
    struct stat stat_path, stat_entry;
    struct dirent *entry;

    stat(path, &stat_path);

    if (S_ISDIR(stat_path.st_mode) == 0) {
        fprintf(stderr, "%s: %s\n", "Is not directory", path);
        exit(-1);
    }

    if ((dir = opendir(path)) == NULL) {
        fprintf(stderr, "%s: %s\n", "Can`t open directory", path);
        exit(-1);
    }

    path_len = strlen(path);

    while ((entry = readdir(dir)) != NULL) {

        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, ".."))
            continue;

        full_path = calloc(path_len + strlen(entry->d_name) + 1, sizeof(char));
        strcpy(full_path, path);
        strcat(full_path, "/");
        strcat(full_path, entry->d_name);

        stat(full_path, &stat_entry);

        if (S_ISDIR(stat_entry.st_mode) != 0) {
            rmtree(full_path);
            continue;
        }

        if (unlink(full_path) == 0)
            printf("Removed a file: %s\n", full_path);
        else
            printf("Can`t remove a file: %s\n", full_path);
        free(full_path);
    }

    if (rmdir(path) == 0)
        printf("Removed a directory: %s\n", path);
    else
        printf("Can`t remove a directory: %s\n", path);

    closedir(dir);
}
```

Jika melakukan rename

```c
static int xmp_rmdir (const char *name)
{
	char *nameFold = (char*) malloc(strlen(name));
	strcpy(nameFold, name);
	update_log("RMDIR", nameFold);
	char *path= (char*) malloc(strlen(dirpath) + strlen(name) + 2);
	sprintf(path, "%s%s",dirpath,name);
	rmtree(path);
	return 0;
}
```

Update log biasa

```c

int update_log(char *arg, const char *desc)
{
	time_t rawtime;
	struct tm *info;

	time( &rawtime );
	info = localtime( &rawtime );
	
	char infoF[10];
	if(strcmp(arg, "RMDIR") == 0)
	{
		strcpy(infoF, "WARNING");
	}
	else
	{
		strcpy(infoF, "INFO");
	}
	
	FILE* file_ptr = fopen(dirLog, "a");	
	fprintf(file_ptr, "%s::%d%d%d-%02d:%02d:%02d::%s::%s\n", infoF, info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, arg, desc);
	
    fclose(file_ptr);

    return 0;
}
```

Update log saat rename

```c
int update_log_rename(const char *desc1, const char *desc2)
{
	time_t rawtime;
	struct tm *info;
	time( &rawtime );
	info = localtime( &rawtime );
	
	FILE* file_ptr = fopen(dirLog, "a");	
	
	fprintf(file_ptr, "INFO::%d%d%d-%02d:%02d:%02d::RENAME::%s::%s\n", info->tm_mday, info->tm_mon+1, info->tm_year+1900, info->tm_hour, info->tm_min, info->tm_sec, desc1, desc2);
	
    fclose(file_ptr);
    return 0;
}
```
